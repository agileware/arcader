#include "Arduino.h"
#include <FormattingSerialDebug.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>
#include <Fonts/FreeSans9pt7b.h>
#include "RGBColor.h"

#define MATRIX_BRIGHTNESS 128
#define MATRIX_WIDTH 8
#define MATRIX_HEIGHT 8
#define MATRIX_PIN 3
#define BUTTON_PIN 4
#define PULSE_MAX 50
#define TIME_MINUTES 5

#define CLOCK_CURSOR 0

Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(MATRIX_WIDTH, MATRIX_HEIGHT, MATRIX_PIN,
		NEO_MATRIX_BOTTOM + NEO_MATRIX_RIGHT + NEO_MATRIX_COLUMNS + NEO_MATRIX_ZIGZAG,
		NEO_GRB + NEO_KHZ800);

const uint16_t colors[] = {
		matrix.Color(255, 0, 0),
		matrix.Color(0, 255, 0),
		matrix.Color(255, 255, 0),
		matrix.Color(0, 255, 255),
		matrix.Color(255, 0, 255),
		matrix.Color(0, 0, 255)
};

void setup() {
	SERIAL_DEBUG_SETUP(9600);
	matrix.begin();
	matrix.setTextWrap(false);
	matrix.setTextColor(colors[0]);
	pinMode(BUTTON_PIN, INPUT_PULLUP);
}

int x = matrix.width();
int pass = 1;
unsigned long time = 0;
bool clock = false;

char buf[5];
char* format(char *fmt, ... ){
        va_list args;
        va_start (args, fmt );
        vsnprintf(buf, 5, fmt, args);
        va_end (args);
        return buf;
}

void loop() {
	DEBUG(F("LOOP"));
	matrix.setBrightness(MATRIX_BRIGHTNESS);
	if (!clock) {
		time = millis();
		matrix.fillScreen(0);
		matrix.setCursor(x, 0);
		matrix.print(F("Innovaction"));
		if (--x < -60) {
			x = matrix.width();
			matrix.setTextColor(colors[pass++ % 6]);
			DEBUG(F("Innovaction"));
		}
		matrix.show();
		while (millis() - time < 100) {
			if (digitalRead(BUTTON_PIN) == LOW) {
				clock = true;
			}
		}
	} else {
		DEBUG("clock start");
		unsigned int min = 0;
		unsigned int sec = 0;
		matrix.fillScreen(0);
		matrix.setCursor(CLOCK_CURSOR, 0);
		matrix.print(F("00:00"));
		matrix.show();
		bool out = true;
		while (digitalRead(BUTTON_PIN) == LOW) {
			// loop until released
			if (matrix.getBrightness() < 5 || matrix.getBrightness() > MATRIX_BRIGHTNESS) {
				out = !out;
			}
			matrix.setBrightness(matrix.getBrightness() + (out ? -1 : 1));
			DEBUG("clock wait %u", matrix.getBrightness());
			matrix.show();
			delay(1000 / PULSE_MAX);
		}
		byte red = 0;
		byte green = 254;
		while (min < TIME_MINUTES || digitalRead(BUTTON_PIN) == LOW) {
			time = millis();
			if (digitalRead(BUTTON_PIN) == LOW) {
				DEBUG("clock stop");
				while (digitalRead(BUTTON_PIN) == LOW) {
					delay(1); // wait until released
				}
				break;
			}
			if (red < 254) {
				red++;
			} else if (green > 0) {
				green--;
			}
			DEBUG("clock time %02d:%02d %lu %03d %03d", min, sec, millis(),red, green);
			matrix.setBrightness(MATRIX_BRIGHTNESS);
			matrix.setTextColor(matrix.Color(red, green, 0));
			matrix.fillScreen(0);
			matrix.setCursor(CLOCK_CURSOR, 0);
			matrix.print(format("%02d:%02d", min, sec));
			matrix.show();
			if (min == 4 && sec >= 50) {
				matrix.setTextColor(matrix.Color(255, 0, 0));
				while (millis() - time < 600) {
					delay(1);
				}
				for (byte i = 0; i < 8; i++) {
					matrix.fillScreen(0);
					matrix.setCursor(CLOCK_CURSOR, i);
					matrix.print(format("%02d:%02d", min, sec));
					matrix.show();
					while (millis() - time < 600 + (50 * (i + 1))) {
						delay(1);
					}
				}
			} else {
				while (millis() - time < 1000) {
					matrix.setBrightness(matrix.getBrightness() - 3);
					matrix.show();
					delay(25);
				}
			}
			if (++sec == 60) {
				sec = 0; min++;
			}
		}
		DEBUG("clock end");
		clock = false;
	}
}
